import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular"
import { SnackBar } from "@nstudio/nativescript-snackbar";
import { ApplicationSettings } from "@nativescript/core";

import { EventData } from "@nativescript/core";
import { Page } from "@nativescript/core";
//import { HelloWorldModel } from "./main-view-model";
import { tnsOauthLogin } from '../services/auth-service';

/*export function navigatingTo(args: EventData) {
  const page = <Page>args.object;
  page.bindingContext = new HelloWorldModel();
}*/

/*export function onLoginTap() {
  // trigger our login
  tnsOauthLogin('google');
}*/

@Component({
    moduleId: module.id,
    selector: "ns-login",
    templateUrl: "login.component.html",
})
export class LoginComponent implements OnInit {

    public input: any;

    public constructor(private router: RouterExtensions) {
        this.input = {
            "email": "",
            "password": ""
        }
    }

    public ngOnInit() {
        if(ApplicationSettings.getBoolean("authenticated", false)) {
            this.router.navigate(["/secure"], { clearHistory: true });
        }
    }

    public login() {
        if(this.input.email && this.input.password) {
            let account = JSON.parse(ApplicationSettings.getString("account", "{}"));
            if(this.input.email == account.email && this.input.password == account.password) {
                ApplicationSettings.setBoolean("authenticated", true);
                this.router.navigate(["/secure"], { clearHistory: true });
            } else {
                (new SnackBar()).simple("Incorrect Credentials!");
            }
        } else {
            (new SnackBar()).simple("All Fields Required!");
        }
    }

    public onLoginTap() {
        // trigger our login
        tnsOauthLogin('google');
        
    }

}