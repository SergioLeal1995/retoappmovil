import { valueFieldProperty } from "@nativescript/core/ui/list-picker/list-picker-common";
import { configureTnsOAuth, TnsOAuthClient, ITnsOAuthTokenResult } from "nativescript-oauth2";
import { ApplicationSettings } from "@nativescript/core";
import { RouterExtensions } from "@nativescript/angular"
 

import {
  TnsOaProvider,
  TnsOaProviderOptionsGoogle,
  TnsOaProviderGoogle
} from "nativescript-oauth2/providers";

let client: TnsOAuthClient = null;
//global.router = router: RouterExtensions;

export function configureOAuthProviders() {
  const googleProvider = configureOAuthProviderGoogle();
  configureTnsOAuth([googleProvider]);
}

function configureOAuthProviderGoogle() {
  const googleProviderOptions: TnsOaProviderOptionsGoogle = {
    openIdSupport: 'oid-full',
    clientId: '790850303877-m7neadt66ofqcoh8puqpue0mv9hc3oq9.apps.googleusercontent.com',
    redirectUri: 'com.googleusercontent.apps.790850303877-m7neadt66ofqcoh8puqpue0mv9hc3oq9:/auth',
    urlScheme: 'com.googleusercontent.apps.790850303877-m7neadt66ofqcoh8puqpue0mv9hc3oq9',
    scopes: ['email']
  };
  
  const googleProvider = new TnsOaProviderGoogle(googleProviderOptions);
  
  return googleProvider;
}

export function tnsOauthLogin(providerType) {
  client = new TnsOAuthClient(providerType);
  
  client.loginWithCompletion((tokenResult: ITnsOAuthTokenResult, error) => {
    if (error) {
      console.error('there was an error logging in.');
      console.error(error);
    } else {
      console.log('logged in sucessfully.');
      console.log(tokenResult);
    }
  });
}