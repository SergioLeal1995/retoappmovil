import { Component } from '@angular/core'
import { configureOAuthProviders } from "../app/components/services/auth-service";

configureOAuthProviders();

@Component({
  selector: 'ns-app',
  templateUrl: './app.component.html',
})


export class AppComponent {}
